package fr.ChadOW.cinventory.CContent.listeners;

import fr.ChadOW.cinventory.CContent.CInventory;
import fr.ChadOW.cinventory.CContent.CItem;
import fr.ChadOW.cinventory.CContent.CUtils;
import fr.ChadOW.cinventory.interfaces.events.clickContent.Action;
import fr.ChadOW.cinventory.interfaces.events.clickContent.ClickContext;
import fr.ChadOW.cinventory.interfaces.events.clickContent.ClickType;
import fr.ChadOW.cinventory.interfaces.representations.Item;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;

public class CInventoryListener implements Listener {

    @EventHandler
    public void onClick(InventoryClickEvent event) {
        if (event.getWhoClicked() instanceof Player) {
            Player player = (Player) event.getWhoClicked();

            CInventory cInventory = CUtils.getCInventoryOfPlayer(player);
            if (cInventory != null && event.getClickedInventory() != null) {
                event.setCancelled(true);
                final ClickContext clickContext = new ClickContext(Action.valueOf(event.getAction().toString()), ClickType.valueOf(event.getClick().toString()));
                if (event.getClickedInventory().equals(cInventory.getInventory())) {

                    final Item item = cInventory.getElement(event.getSlot());

                    cInventory.getClicksEvents().forEach(itemClickEvent -> itemClickEvent.onClick(cInventory, item, player, clickContext));

                    if (item != null) {
                        if (item instanceof CItem && ((CItem) item).isPickable()) {
                            event.setCancelled(false);
                            cInventory.removeElementWithoutClear(item);
                        }
                        item.getEvents().forEach(callBack -> callBack.onClick(cInventory, item, player, clickContext));
                    }
                } else if (!clickContext.getClickType().isShiftClick())
                    event.setCancelled(false);
            }
        }
    }

    @EventHandler
    public void onClose(InventoryCloseEvent event) {
        if (event.getPlayer() instanceof Player) {
            Player player = (Player) event.getPlayer();

            CInventory cInventory = CUtils.getCInventoryOfPlayer(player);
            if (cInventory != null) {

                if (!cInventory.isClosable())
                    Bukkit.getScheduler().runTaskLater(CUtils.getInstance(), () -> cInventory.open(player), 1 );
            }
        }
    }
}
