package fr.ChadOW.cinventory.CContent;

import fr.ChadOW.cinventory.interfaces.events.InventoryCloseEvent;
import fr.ChadOW.cinventory.interfaces.events.ItemClickEvent;
import fr.ChadOW.cinventory.interfaces.representations.Inventory;
import fr.ChadOW.cinventory.interfaces.representations.Item;
import org.bukkit.Bukkit;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CInventory implements Inventory {

    private org.bukkit.inventory.Inventory inventory;
    private boolean closable;

    private final ArrayList<Item> items;
    private final ArrayList<InventoryCloseEvent> closeEvents;
    private final ArrayList<ItemClickEvent> clicksEvents;

    public CInventory(int size, String name) {
        inventory = Bukkit.createInventory(null, size, name);

        items = new ArrayList<>();
        closeEvents = new ArrayList<>();
        clicksEvents = new ArrayList<>();
        closable = true;

        CUtils.cInventories.add(this);
    }

    public void open(Player player) {
        if (!inventory.getViewers().contains(player)) {
            player.closeInventory();
            player.openInventory(inventory);
        }
    }

    public void close(Player player) {
        if (isClosable()) {
            getCloseEvents().forEach(callBack -> callBack.onClick(this, player));
            player.closeInventory();
        }
    }


    public void addElement(Item item) {
        if (!items.contains(item)) {
            items.add(item);
            item.getInventories().add(this);

            updateDisplay();
        }
    }
    public void removeElement(Item item) {
        removeElementWithoutClear(item);
        inventory.clear(item.getSlot());
    }
    public void removeElementWithoutClear(Item item) {
        items.remove(item);
        item.getInventories().remove(this);
    }

    public void addCloseEvent(InventoryCloseEvent event) {
        closeEvents.add(event);
    }

    public void removeCloseEvent(InventoryCloseEvent event) {
        closeEvents.remove(event);
    }

    public List<InventoryCloseEvent> getCloseEvents() {
        return closeEvents;
    }

    public void addClickEvent(ItemClickEvent event) {
        clicksEvents.add(event);
    }

    public void removeClickEvent(ItemClickEvent event) {
        clicksEvents.remove(event);
    }

    public List<ItemClickEvent> getClicksEvents() {
        return clicksEvents;
    }

    public Item getElement(int slot) {
        List<Item> copy = new ArrayList<>(items);
        Collections.reverse(copy);
        for (Item item : copy) {
            if (item.getSlot() == slot)
                return item;
        }
        return null;
    }


    public void updateDisplay() {
        inventory.clear();
        for (Item item : items) {
            updateDisplayOfItem(item);
        }
    }

    public void updateDisplayOfItem(Item item) {
        if (item.getSlot() < inventory.getSize())
            inventory.setItem(item.getSlot(), item.build());
    }

    public void setSize(int size) {
        List<Player> players = getPlayers();
        players.forEach(HumanEntity::closeInventory);
        inventory = Bukkit.createInventory(null, size, inventory.getType().getDefaultTitle());
        updateDisplay();
        players.forEach(this::open);
    }

    public void setName(String name) {
        List<Player> players = getPlayers();
        players.forEach(HumanEntity::closeInventory);
        inventory = Bukkit.createInventory(null, inventory.getSize(), name);
        updateDisplay();
        players.forEach(this::open);
    }

    public org.bukkit.inventory.Inventory getInventory() {
        return inventory;
    }

    public boolean isClosable() {
        return closable;
    }

    public void setClosable(boolean closable) {
        this.closable = closable;
    }

    public List<Player> getPlayers() {
        List<Player> players = new ArrayList<>();
        inventory.getViewers().forEach(humanEntity -> {
            players.add((Player) humanEntity);
        });
        return players;
    }

    public List<Item> getItems() {
        return items;
    }
}
