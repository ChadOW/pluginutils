package fr.ChadOW.cinventory.CContent;

import fr.ChadOW.cinventory.interfaces.representations.Inventory;
import fr.ChadOW.cinventory.interfaces.ItemCreator;
import fr.ChadOW.cinventory.interfaces.events.ItemClickEvent;
import fr.ChadOW.cinventory.interfaces.representations.Item;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;

public class CItem implements Item {

    private int slot;
    private boolean pickable;

    private ItemCreator item;
    private final List<ItemClickEvent> events;
    private final List<Inventory> inventories;

    public CItem(ItemCreator item) {
        this.item = item;
        pickable = false;
        events = new ArrayList<>();
        inventories = new ArrayList<>();

        slot = 0;
    }

    public CItem setSlot(int slot) {
        List<Inventory> inventories = new ArrayList<>(getInventories());

        for (Inventory inventory : inventories) {
            inventory.removeElement(this);
        }

        this.slot = slot;

        for (Inventory inventory : inventories)
            inventory.addElement(this);
        return this;
    }

    public int getSlot() {
        return slot;
    }

    public CItem setName(String name) {
        item.setName(name);
        return this;
    }

    public String getName() {
        return item.getName();
    }

    public CItem setDescription(List<String> description) {
        item.setLores(description);
        return this;
    }

    public List<String> getDescription() {
        return item.getLores();
    }

    public CItem setMaterial(Material material) {
        item.setMaterial(material);
        return this;
    }

    public Material getMaterial() {
        return item.getMaterial();
    }

    public CItem setAmount(int amount) {
        item.setAmount(amount);
        return this;
    }

    public int getAmount() {
        return item.getAmount();
    }


    public CItem setItemCreator(ItemCreator itemCreator) {
        this.item = itemCreator;
        return this;
    }

    public ItemCreator getItemCreator() {
        return item;
    }

    public boolean isPickable() {
        return pickable;
    }

    public CItem setPickable(boolean canBePickedUp) {
        this.pickable = canBePickedUp;
        return this;
    }

    public CItem addEvent(ItemClickEvent event) {
        events.add(event);
        return this;
    }

    public void removeEvent(ItemClickEvent event) {
        events.remove(event);
    }

    public List<ItemClickEvent> getEvents() {
        return events;
    }


    public ItemStack build() {
        return item.getItem();
    }

    public CItem updateDisplay() {
        for (Inventory inventory : inventories) {
            inventory.updateDisplay();
        }
        return this;
    }

    public List<Inventory> getInventories() {
        return inventories;
    }
}
