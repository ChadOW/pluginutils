package fr.ChadOW.cinventory.CContent;

import fr.ChadOW.cinventory.CContent.listeners.CInventoryListener;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;

public class CUtils {
    private static final String dev_name = "[ChadOW] §7";
    private static boolean isInit = false;

    private static JavaPlugin INSTANCE;
    static final ArrayList<CInventory> cInventories = new ArrayList<>();

    public static void init(JavaPlugin i) {
        if (!isInit) {
            INSTANCE = i;
            isInit = true;
            CommandSender sender = INSTANCE.getServer().getConsoleSender();

            INSTANCE.getServer().getPluginManager().registerEvents(new CInventoryListener(), INSTANCE);
            sender.sendMessage(dev_name + "CInventory enable ! Thank you for using my API !");
        }
    }


    public static CInventory getCInventoryOfPlayer(Player player) {
        for (CInventory cInventory : cInventories) {
            if (cInventory.getPlayers().contains(player))
                return cInventory;
        }
        return null;
    }

    public static boolean hasCInventoryOpen(Player player) {
        for (CInventory cInventory : cInventories) {
            if (cInventory.getPlayers().contains(player))
                return true;
        }
        return false;
    }

    public static JavaPlugin getInstance() {
        return INSTANCE;
    }
}
