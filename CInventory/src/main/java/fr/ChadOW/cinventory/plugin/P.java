package fr.ChadOW.cinventory.plugin;

import fr.ChadOW.cinventory.CContent.CInventory;
import fr.ChadOW.cinventory.CContent.CItem;
import fr.ChadOW.cinventory.CContent.CUtils;
import fr.ChadOW.cinventory.interfaces.ItemCreator;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.Arrays;

public class P extends JavaPlugin implements CommandExecutor {

    public static final String prefix = "§6[Worlds] §f";
    private static CInventory worldGUI;

    @Override
    public void onEnable() {
        CUtils.init(this);
        createExempleGUI();
        getCommand("cinventorytest").setExecutor(this);
    }

    private void createExempleGUI() {
        worldGUI = new CInventory(45, "§eMenu des mondes");

        worldGUI.addElement(new CItem(new ItemCreator(Material.COMPASS, 0)
                .setName("§6Serveur libre")
                .setLores(Arrays.asList(
                        "§7",
                        "§fCliquez pour vous connecter au serveur"
                )))
                .addEvent((inventoryRepresentation, itemRepresentation, player, clickContext) -> {
                    inventoryRepresentation.close(player);
                    player.sendMessage(prefix + "Connexion vers §aserveur libre§f.");
                }).setSlot(11));
        worldGUI.addElement(new CItem(new ItemCreator(Material.DIAMOND_PICKAXE, 0)
                .setName("§6Serveur ressources")
                .setLores(Arrays.asList(
                        "§7",
                        "§fCliquez pour vous connecter au serveur"
                )))
                .addEvent((inventoryRepresentation, itemRepresentation, player, clickContext) -> {
                    inventoryRepresentation.close(player);
                    player.sendMessage(prefix + "Connexion vers §aserveur ressources§f.");
                }).setSlot(13));
        worldGUI.addElement(new CItem(new ItemCreator(Material.REDSTONE, 0)
                .setName("§cAUGMENTER LA TAILLE")
                .setLores(Arrays.asList(
                        "§7",
                        "§fCliquez pour vous connecter au serveur"
                )))
                .addEvent((inventoryRepresentation, itemRepresentation, player, clickContext) -> inventoryRepresentation.setSize(45)).setSlot(15));

        worldGUI.addElement(new CItem(new ItemCreator(Material.GRASS_BLOCK, 0)
                .setName("§6OverWorld")
                .setLores(Arrays.asList(
                        "§7",
                        "§fCliquez pour vous téléporter au spawn du monde normal"
                )))
                .addEvent((inventoryRepresentation, itemRepresentation, player, clickContext) -> {
                    inventoryRepresentation.close(player);
                    player.sendMessage(prefix + "Téléportation vers §amonde normal§f.");
                }).setSlot(29));
        worldGUI.addElement(new CItem(new ItemCreator(Material.NAME_TAG, 0)
                .setName("§cCHANGER LE NOM")
                .setLores(Arrays.asList(
                        "§7",
                        "§fCliquez pour vous téléporter au spawn du monde nether"
                )))
                .addEvent((inventoryRepresentation, itemRepresentation, player, clickContext) -> inventoryRepresentation.setName("§etestNouveauNom")).setSlot(31));
        worldGUI.addElement(new CItem(new ItemCreator(Material.REDSTONE, 0)
                .setName("§cREDUIRE LA TAILLE")
                .setLores(Arrays.asList(
                        "§7",
                        "§fCliquez pour vous téléporter au spawn du monde end"
                )))
                .addEvent((inventoryRepresentation, itemRepresentation, player, clickContext) -> inventoryRepresentation.setSize(18)).setSlot(33));

        worldGUI.addElement(new CItem(new ItemCreator(Material.EMERALD, 0)
                .setName("§aPick Me :D")
                .setLores(Arrays.asList(
                        "§7",
                        "§7On peut me récuperer"
                )))
                .setPickable(true)
                .addEvent((paramInventory, paramItem, paramPlayer, paramClickContext) -> {
                    paramPlayer.sendMessage("§eYou found a treasure !");
                }).setSlot(22));

        worldGUI.addElement(new CItem(new ItemCreator(Material.ORANGE_STAINED_GLASS_PANE, 0).setName("§f")).setSlot(0));
        worldGUI.addElement(new CItem(new ItemCreator(Material.ORANGE_STAINED_GLASS_PANE, 0).setName("§f")).setSlot(1));
        worldGUI.addElement(new CItem(new ItemCreator(Material.WHITE_STAINED_GLASS_PANE, 0).setName("§f")).setSlot(2));
        worldGUI.addElement(new CItem(new ItemCreator(Material.ORANGE_STAINED_GLASS_PANE, 0).setName("§f")).setSlot(3));
        worldGUI.addElement(new CItem(new ItemCreator(Material.WHITE_STAINED_GLASS_PANE, 0).setName("§f")).setSlot(4));
        worldGUI.addElement(new CItem(new ItemCreator(Material.ORANGE_STAINED_GLASS_PANE, 0).setName("§f")).setSlot(5));
        worldGUI.addElement(new CItem(new ItemCreator(Material.WHITE_STAINED_GLASS_PANE, 0).setName("§f")).setSlot(6));
        worldGUI.addElement(new CItem(new ItemCreator(Material.ORANGE_STAINED_GLASS_PANE, 0).setName("§f")).setSlot(7));
        worldGUI.addElement(new CItem(new ItemCreator(Material.ORANGE_STAINED_GLASS_PANE, 0).setName("§f")).setSlot(8));

        worldGUI.addElement(new CItem(new ItemCreator(Material.WHITE_STAINED_GLASS_PANE, 0).setName("§f")).setSlot(9));
        worldGUI.addElement(new CItem(new ItemCreator(Material.WHITE_STAINED_GLASS_PANE, 0).setName("§f")).setSlot(17));

        worldGUI.addElement(new CItem(new ItemCreator(Material.ORANGE_STAINED_GLASS_PANE, 0).setName("§f")).setSlot(18));
        worldGUI.addElement(new CItem(new ItemCreator(Material.ORANGE_STAINED_GLASS_PANE, 0).setName("§f")).setSlot(19));
        worldGUI.addElement(new CItem(new ItemCreator(Material.WHITE_STAINED_GLASS_PANE, 0).setName("§f")).setSlot(20));
        worldGUI.addElement(new CItem(new ItemCreator(Material.ORANGE_STAINED_GLASS_PANE, 0).setName("§f")).setSlot(21));
        worldGUI.addElement(new CItem(new ItemCreator(Material.ORANGE_STAINED_GLASS_PANE, 0).setName("§f")).setSlot(23));
        worldGUI.addElement(new CItem(new ItemCreator(Material.WHITE_STAINED_GLASS_PANE, 0).setName("§f")).setSlot(24));
        worldGUI.addElement(new CItem(new ItemCreator(Material.ORANGE_STAINED_GLASS_PANE, 0).setName("§f")).setSlot(25));
        worldGUI.addElement(new CItem(new ItemCreator(Material.ORANGE_STAINED_GLASS_PANE, 0).setName("§f")).setSlot(26));

        worldGUI.addElement(new CItem(new ItemCreator(Material.WHITE_STAINED_GLASS_PANE, 0).setName("§f")).setSlot(27));
        worldGUI.addElement(new CItem(new ItemCreator(Material.WHITE_STAINED_GLASS_PANE, 0).setName("§f")).setSlot(35));

        worldGUI.addElement(new CItem(new ItemCreator(Material.ORANGE_STAINED_GLASS_PANE, 0).setName("§f")).setSlot(36));
        worldGUI.addElement(new CItem(new ItemCreator(Material.ORANGE_STAINED_GLASS_PANE, 0).setName("§f")).setSlot(37));
        worldGUI.addElement(new CItem(new ItemCreator(Material.WHITE_STAINED_GLASS_PANE, 0).setName("§f")).setSlot(38));
        worldGUI.addElement(new CItem(new ItemCreator(Material.ORANGE_STAINED_GLASS_PANE, 0).setName("§f")).setSlot(39));
        worldGUI.addElement(new CItem(new ItemCreator(Material.WHITE_STAINED_GLASS_PANE, 0).setName("§f")).setSlot(40));
        worldGUI.addElement(new CItem(new ItemCreator(Material.ORANGE_STAINED_GLASS_PANE, 0).setName("§f")).setSlot(41));
        worldGUI.addElement(new CItem(new ItemCreator(Material.WHITE_STAINED_GLASS_PANE, 0).setName("§f")).setSlot(42));
        worldGUI.addElement(new CItem(new ItemCreator(Material.ORANGE_STAINED_GLASS_PANE, 0).setName("§f")).setSlot(43));
        worldGUI.addElement(new CItem(new ItemCreator(Material.ORANGE_STAINED_GLASS_PANE, 0).setName("§f")).setSlot(44));
    }


    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (sender instanceof Player)
            worldGUI.open((Player) sender);
        return true;
    }
}
