package fr.ChadOW.cinventory.interfaces.representations;

import org.bukkit.entity.Player;

public interface Inventory {
    void open(Player player);
    void close(Player player);

    void addElement(Item item);
    void removeElement(Item item);

    void updateDisplay();
    void updateDisplayOfItem(Item item);

    Item getElement(int slot);

    void setSize(int size);

    void setName(String name);
}
