package fr.ChadOW.cinventory.interfaces;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.block.banner.Pattern;
import org.bukkit.block.banner.PatternType;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.*;

public class ItemCreator {

    public enum BannerPreset {
        barre, precedent, suivant, coeur, cercleEtoile, croix, yinYang, losange, moin, plus;
    }

    public enum ComparatorType {
        All, ItemStack, Similar, Material, Amount, Durability, Name, Lores, Enchantements, ItemsFlags, Owner, BaseColor, Patterns, StoredEnchantements, Possesseur, Creator_Name, TAG;
    }

    private static Material skullMat;
    private static Material bannerMat;

    private static String extractVersion() {
        String[] strings = Bukkit.getBukkitVersion().split("-");
        if (strings.length > 0) {
            return strings[0];
        }
        return "?";
    }

    private static void getClasses() {
        if (skullMat == null || bannerMat == null) {
            String version = extractVersion();
            if (version.startsWith("1.8.") || version.startsWith("1.9.") || version.startsWith("1.10.") || version.startsWith("1.11.") || version.startsWith("1.12.")) {
                skullMat = Material.valueOf("SKULL_ITEM");
                bannerMat = Material.valueOf("BANNER");
            } else {
                skullMat = Material.valueOf("PLAYER_HEAD");
                bannerMat = Material.valueOf("WHITE_BANNER");
            }
        }
    }

    private ItemStack item;
    private Player possesseur;
    private String creator_name;
    private ArrayList<String> tag;

    private ArrayList<Pattern> patterns;

    public ItemCreator(final Material material, final int byteID) {
        getClasses();
        item = new ItemStack(material, 1, (byte) byteID);
    }

    public ItemCreator(final ItemStack item) {
        getClasses();
        setMaterial(item.getType());
        setAmount(item.getAmount());
        setDurability(item.getDurability());
        setName(item.getItemMeta().getDisplayName());
        setEnchantments(item.getItemMeta().getEnchants());
        setLores(item.getItemMeta().getLore());
    }

    public ItemCreator(final ItemCreator itemcreator) {
        getClasses();
        this.item = itemcreator.getItem();
        this.possesseur = itemcreator.getPossesseur();
        this.creator_name = itemcreator.getCreator_name();
        this.tag = new ArrayList<>(itemcreator.getTag());
    }

    public ItemStack getItem() {
        return item;
    }

    public Material getMaterial() {
        return item.getType();
    }

    public ItemCreator setUnbreakable(final Boolean unbreakable) {
        final ItemMeta meta = item.getItemMeta();
        //meta.setUnbreakable(unbreakable);
        item.setItemMeta(meta);
        return this;
    }

    public ItemCreator setMaterial(final Material material) {
        if (item == null) {
            item = new ItemStack(material);
        } else {
            item.setType(material);
        }
        return this;
    }

    public Integer getAmount() {
        return item.getAmount();
    }

    public ItemCreator setAmount(final Integer amount) {
        item.setAmount(amount);
        return this;
    }

    public Short getDurability() {
        return item.getDurability();
    }

    public Integer getDurabilityInteger() {
        return (int) item.getDurability();
    }

    public ItemCreator setDurability(final Short durability) {
        item.setDurability(durability);
        return this;
    }

    public ItemCreator setDurability(final Integer durability) {
        final Short shortdurability = ((short) ((int) durability));
        item.setDurability(shortdurability);
        return this;
    }

    public ItemMeta getMeta() {
        return item.getItemMeta();
    }

    public ItemCreator setMeta(final ItemMeta meta) {
        item.setItemMeta(meta);
        return this;
    }

    public String getName() {
        return item.getItemMeta().getDisplayName();
    }

    public ItemCreator setName(final String name) {
        final ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(name);
        item.setItemMeta(meta);
        return this;
    }

    public ArrayList<String> getLores() {
        return (ArrayList<String>) item.getItemMeta().getLore();
    }

    public ItemCreator setLores(final List<String> list) {
        final ItemMeta meta = item.getItemMeta();
        meta.setLore(list);
        item.setItemMeta(meta);
        return this;
    }

    public ItemCreator clearLores() {
        final ItemMeta meta = item.getItemMeta();
        meta.setLore(new ArrayList<String>());
        item.setItemMeta(meta);
        return this;
    }

    public ItemCreator insertLores(final String lore, final Integer position) {
        final ItemMeta meta = item.getItemMeta();
        ArrayList<String> lores = (ArrayList<String>) meta.getLore();
        if (lores == null) {
            lores = new ArrayList<>();
        }
        lores.add(position, lore);
        meta.setLore(lores);
        item.setItemMeta(meta);
        return this;
    }

    public ItemCreator addLore(final String lore) {
        final ItemMeta meta = item.getItemMeta();
        ArrayList<String> lores = (ArrayList<String>) meta.getLore();
        if (lores == null) {
            lores = new ArrayList<>();
        }
        if (lore != null) {
            lores.add(lore);
        } else {
            lores.add(" ");
        }
        meta.setLore(lores);
        item.setItemMeta(meta);
        return this;
    }

    public ItemCreator removeLore(final String lore) {
        final ItemMeta meta = item.getItemMeta();
        final ArrayList<String> lores = (ArrayList<String>) meta.getLore();
        if (lores != null) {
            if (lores.contains(lore)) {
                lores.remove(lore);
                meta.setLore(lores);
                item.setItemMeta(meta);
            }
        }
        return this;
    }

    public String[] getTableauLores() {
        final String[] tableaulores = new String[] {};
        if (item.getItemMeta().getLore() != null) {
            Integer i = 0;
            for (final String lore : item.getItemMeta().getLore()) {
                tableaulores[i] = lore;
                i++;
            }
        }
        return tableaulores;
    }

    public ItemCreator setTableauLores(final String[] lores) {
        final ArrayList<String> tableaulores = new ArrayList<>();
        for (final String lore : lores) {
            tableaulores.add(lore);
        }
        final ItemMeta meta = item.getItemMeta();
        meta.setLore(tableaulores);
        item.setItemMeta(meta);
        return this;
    }

    @SuppressWarnings("unlikely-arg-type")
    public ItemCreator replaceallLores(final String replacelore, final String newlore) {
        final ItemMeta meta = item.getItemMeta();
        final ArrayList<String> lores = (ArrayList<String>) meta.getLore();
        if (lores != null) {
            if (lores.contains(replacelore)) {
                for (Integer i = 0; i < lores.size(); i++) {
                    final String lore = lores.get(i);
                    if (lore.equals(replacelore)) {
                        lores.remove(i);
                        lores.add(i, newlore);
                    }
                }
                meta.setLore(lores);
                item.setItemMeta(meta);
            }
        }
        return this;
    }

    @SuppressWarnings("unlikely-arg-type")
    public ItemCreator replaceoneLore(final Integer ligne, final String newlore) {
        final ItemMeta meta = item.getItemMeta();
        final ArrayList<String> lores = (ArrayList<String>) meta.getLore();
        if (lores != null) {
            if (lores.get(ligne) != null) {
                lores.remove(ligne);
                lores.add(ligne, newlore);
                meta.setLore(lores);
                item.setItemMeta(meta);
            }
        }
        return this;
    }

    @SuppressWarnings("unlikely-arg-type")
    public ItemCreator replacefirstLores(final String replacelore, final String newlore, final Integer nombre) {
        final ItemMeta meta = item.getItemMeta();
        final ArrayList<String> lores = (ArrayList<String>) meta.getLore();
        if (lores != null) {
            if (lores.contains(replacelore)) {
                Integer replaced = 0;
                for (Integer i = 0; i < lores.size(); i++) {
                    if (lores.get(i).equals(replacelore)) {
                        lores.remove(i);
                        lores.add(i, newlore);
                        replaced++;
                        if (replaced >= nombre) {
                            break;
                        }
                    }
                }
                meta.setLore(lores);
                item.setItemMeta(meta);
            }
        }
        return this;
    }

    @SuppressWarnings({ "unlikely-arg-type" })
    public ItemCreator replacelastLores(final String replacelore, final String newlore, final Integer nombre) {
        final ItemMeta meta = item.getItemMeta();
        final ArrayList<String> lores = (ArrayList<String>) meta.getLore();
        if (lores != null) {
            if (lores.contains(replacelore)) {
                Integer replaced = 0;
                for (Integer i = lores.size() - 1; i >= 0; i--) {
                    if (lores.get(i).equals(replacelore)) {
                        lores.remove(i);
                        lores.add(i, newlore);
                        replaced++;
                        if (replaced >= nombre) {
                            break;
                        }
                    }
                }
                meta.setLore(lores);
                item.setItemMeta(meta);
            }
        }
        return this;
    }

    public HashMap<Enchantment, Integer> getEnchantments() {
        return new HashMap<Enchantment, Integer>(item.getItemMeta().getEnchants());
    }

    public ItemCreator setEnchantments(final Map<Enchantment, Integer> map) {
        final ItemMeta meta = item.getItemMeta();
        if (meta.getEnchants() != null) {
            final ArrayList<Enchantment> cloneenchantments = new ArrayList<>(meta.getEnchants().keySet());
            for (final Enchantment enchantment : cloneenchantments) {
                meta.removeEnchant(enchantment);
            }
        }
        for (final Entry<Enchantment, Integer> e : map.entrySet()) {
            meta.addEnchant(e.getKey(), e.getValue(), true);
        }
        item.setItemMeta(meta);
        return this;
    }

    public ItemCreator clearEnchantments() {
        final ItemMeta meta = item.getItemMeta();
        if (meta.getEnchants() != null) {
            final ArrayList<Enchantment> cloneenchantments = new ArrayList<>(meta.getEnchants().keySet());
            for (final Enchantment enchantment : cloneenchantments) {
                meta.removeEnchant(enchantment);
            }
            item.setItemMeta(meta);
        }
        return this;
    }

    public ItemCreator addEnchantment(final Enchantment enchantment, final Integer lvl) {
        final ItemMeta meta = item.getItemMeta();
        meta.addEnchant(enchantment, lvl, true);
        item.setItemMeta(meta);
        return this;
    }

    public ItemCreator removeEnchantment(final Enchantment enchantment) {
        final ItemMeta meta = item.getItemMeta();
        if (meta.getEnchants() != null) {
            if (meta.getEnchants().containsKey(enchantment)) {
                meta.removeEnchant(enchantment);
                item.setItemMeta(meta);
            }
        }
        return this;
    }

    public Enchantment[] getTableauEnchantments() {
        final Enchantment[] enchantments = new Enchantment[] {};
        if (item.getItemMeta().getEnchants() != null) {
            Integer i = 0;
            for (final Enchantment enchantment : item.getItemMeta().getEnchants().keySet()) {
                enchantments[i] = enchantment;
                i++;
            }
        }
        return enchantments;
    }

    public Integer[] getTableauEnchantmentslvl() {
        final Integer[] enchantmentslvl = new Integer[] {};
        if (item.getItemMeta().getEnchants() != null) {
            Integer i = 0;
            for (final Integer enchantmentlvl : item.getItemMeta().getEnchants().values()) {
                enchantmentslvl[i] = enchantmentlvl;
                i++;
            }
        }
        return enchantmentslvl;
    }

    public ItemCreator setTableauEnchantments(final Enchantment[] enchantments, final Integer[] enchantmentslvl) {
        final ItemMeta meta = item.getItemMeta();
        if (meta.getEnchants() != null) {
            final ArrayList<Enchantment> cloneenchantments = new ArrayList<>(meta.getEnchants().keySet());
            for (final Enchantment enchantment : cloneenchantments) {
                meta.removeEnchant(enchantment);
            }
        }
        for (Integer i = 0; i < enchantments.length && i < enchantmentslvl.length; i++) {
            meta.addEnchant(enchantments[i], enchantmentslvl[i], true);
        }
        item.setItemMeta(meta);
        return this;
    }

    public ArrayList<ItemFlag> getItemFlags() {
        final ArrayList<ItemFlag> itemflags = new ArrayList<>();
        if (item.getItemMeta().getItemFlags() != null) {
            for (final ItemFlag itemflag : item.getItemMeta().getItemFlags()) {
                itemflags.add(itemflag);
            }
        }
        return itemflags;
    }

    public ItemCreator setItemFlags(final ArrayList<ItemFlag> itemflags) {
        final ItemMeta meta = item.getItemMeta();
        if (meta.getItemFlags() != null) {
            final ArrayList<ItemFlag> cloneitemflags = new ArrayList<>();
            for (final ItemFlag itemflag : meta.getItemFlags()) {
                cloneitemflags.add(itemflag);
            }
            for (final ItemFlag itemflag : cloneitemflags) {
                meta.removeItemFlags(itemflag);
            }
        }
        for (final ItemFlag itemflag : itemflags) {
            meta.addItemFlags(itemflag);
        }
        item.setItemMeta(meta);
        return this;
    }

    public ItemCreator clearItemFlags() {
        final ItemMeta meta = item.getItemMeta();
        if (meta.getItemFlags() != null) {
            final ArrayList<ItemFlag> cloneitemflags = new ArrayList<>();
            for (final ItemFlag itemflag : meta.getItemFlags()) {
                cloneitemflags.add(itemflag);
            }
            for (final ItemFlag itemflag : cloneitemflags) {
                meta.removeItemFlags(itemflag);
            }
            item.setItemMeta(meta);
        }
        return this;
    }

    public ItemCreator addItemFlags(final ItemFlag itemflag) {
        final ItemMeta meta = item.getItemMeta();
        meta.addItemFlags(itemflag);
        item.setItemMeta(meta);
        return this;
    }

    public ItemCreator removeItemFlags(final ItemFlag itemflag) {
        final ItemMeta meta = item.getItemMeta();
        if (meta.getItemFlags() != null) {
            if (meta.getItemFlags().contains(itemflag)) {
                meta.removeItemFlags(itemflag);
                item.setItemMeta(meta);
            }
        }
        return this;
    }

    public ItemFlag[] getTableauItemFlags() {
        final ItemMeta meta = item.getItemMeta();
        final ItemFlag[] itemflags = new ItemFlag[] {};
        Integer i = 0;
        if (meta.getItemFlags() != null) {
            for (final ItemFlag itemflag : meta.getItemFlags()) {
                itemflags[i] = itemflag;
                i++;
            }
        }
        return itemflags;
    }

    public ItemCreator setTableauItemFlags(final ItemFlag[] itemflags) {
        final ItemMeta meta = item.getItemMeta();
        if (meta.getItemFlags() != null) {
            final ArrayList<ItemFlag> cloneitemflags = new ArrayList<>();
            for (final ItemFlag itemflag : meta.getItemFlags()) {
                cloneitemflags.add(itemflag);
            }
            for (final ItemFlag itemflag : cloneitemflags) {
                meta.removeItemFlags(itemflag);
            }
        }
        for (final ItemFlag itemflag : itemflags) {
            meta.addItemFlags(itemflag);
        }
        item.setItemMeta(meta);
        return this;
    }

    public SkullMeta getSkullMeta() {
        if (item.getType().equals(skullMat)) {
            return (SkullMeta) item.getItemMeta();
        }
        return null;
    }

    public ItemCreator setSkullMeta(final SkullMeta skullmeta) {
        if (item.getType().equals(skullMat)) {
            item.setItemMeta(skullmeta);
        }
        return this;
    }

    public String getOwner() {
        if (item.getType().equals(skullMat)) {
            return ((SkullMeta) item.getItemMeta()).getOwner();
        }
        return null;
    }

    public ItemCreator setOwner(final String owner) {
        if (item.getType().equals(skullMat)) {
            final SkullMeta meta = (SkullMeta) item.getItemMeta();
            meta.setOwner(owner);
            item.setItemMeta(meta);
        }
        return this;
    }

    public BannerMeta getBannerMeta() {
        if (item.getType().equals(bannerMat)) {
            return (BannerMeta) item.getItemMeta();
        }
        return null;
    }

    public ItemCreator setBannerMeta(final BannerMeta bannermeta) {
        if (item.getType().equals(bannerMat)) {
            item.setItemMeta(bannermeta);
        }
        return this;
    }

    public DyeColor getBasecolor() {
        if (item.getType().equals(bannerMat)) {
            return ((BannerMeta) item.getItemMeta()).getBaseColor();
        }
        return null;
    }

    public ItemCreator setBasecolor(final DyeColor basecolor) {
        if (item.getType().equals(bannerMat)) {
            final BannerMeta meta = (BannerMeta) item.getItemMeta();
            meta.setBaseColor(basecolor);
            item.setItemMeta(meta);
        }
        return this;
    }
    public ItemCreator setColor(final Color basecolor) {
        if (item.getType().equals(Material.LEATHER_BOOTS) || item.getType().equals(Material.LEATHER_CHESTPLATE) || item.getType().equals(Material.LEATHER_HELMET) || item.getType().equals(Material.LEATHER_LEGGINGS)) {
            final LeatherArmorMeta meta = (LeatherArmorMeta) item.getItemMeta();
            meta.setColor(basecolor);
            item.setItemMeta(meta);
        }
        return this;
    }

    public ArrayList<Pattern> getPatterns() {
        if (item.getType().equals(bannerMat)) {
            return (ArrayList<Pattern>) ((BannerMeta) item.getItemMeta()).getPatterns();
        }
        return null;
    }

    public ItemCreator setPatterns(final ArrayList<Pattern> petterns) {
        if (item.getType().equals(bannerMat)) {
            final BannerMeta meta = (BannerMeta) item.getItemMeta();
            meta.setPatterns(petterns);
            item.setItemMeta(meta);
        }
        return this;
    }

    public ItemCreator clearPatterns() {
        if (item.getType().equals(bannerMat)) {
            final BannerMeta meta = (BannerMeta) item.getItemMeta();
            meta.setPatterns(new ArrayList<>());
            item.setItemMeta(meta);
        }
        return this;
    }

    public ItemCreator addPattern(final Pattern pattern) {
        if (item.getType().equals(bannerMat)) {
            final BannerMeta meta = (BannerMeta) item.getItemMeta();
            meta.addPattern(pattern);
            item.setItemMeta(meta);
        }
        return this;
    }

    public ItemCreator removePattern(final Pattern pattern) {
        if (item.getType().equals(bannerMat)) {
            final BannerMeta meta = (BannerMeta) item.getItemMeta();
            final ArrayList<Pattern> patterns = (ArrayList<Pattern>) meta.getPatterns();
            if (patterns != null) {
                if (patterns.contains(pattern)) {
                    patterns.remove(pattern);
                    meta.setPatterns(patterns);
                    item.setItemMeta(meta);
                }
            }
        }
        return this;
    }

    public Pattern[] getTableauPatterns() {
        if (item.getType().equals(bannerMat)) {
            final BannerMeta meta = (BannerMeta) item.getItemMeta();
            final Pattern[] tableaupatterns = new Pattern[] {};
            if (meta.getPatterns() != null) {
                Integer i = 0;
                for (final Pattern pattern : meta.getPatterns()) {
                    tableaupatterns[i] = pattern;
                    i++;
                }
            }
            return tableaupatterns;
        }
        return null;
    }

    public ItemCreator setTableauPatterns(final Pattern[] patterns) {
        if (item.getType().equals(bannerMat)) {
            final BannerMeta meta = (BannerMeta) item.getItemMeta();
            if (meta.getPatterns() != null) {
                meta.setPatterns(new ArrayList<>());
            }
            for (final Pattern pattern : patterns) {
                meta.addPattern(pattern);
            }
            item.setItemMeta(meta);
        }
        return this;
    }

    public EnchantmentStorageMeta getEnchantmentStorageMeta() {
        if (item.getType().equals(Material.ENCHANTED_BOOK)) {
            return (EnchantmentStorageMeta) item.getItemMeta();
        }
        return null;
    }

    public ItemCreator setEnchantmentStorageMeta(final EnchantmentStorageMeta enchantmentstoragemeta) {
        if (item.getType().equals(Material.ENCHANTED_BOOK)) {
            item.setItemMeta(enchantmentstoragemeta);
        }
        return this;
    }

    public HashMap<Enchantment, Integer> getStoredEnchantments() {
        if (item.getType().equals(Material.ENCHANTED_BOOK)) {
            return (HashMap<Enchantment, Integer>) ((EnchantmentStorageMeta) item.getItemMeta()).getEnchants();
        }
        return null;
    }

    public ItemCreator setStoredEnchantments(final HashMap<Enchantment, Integer> storedenchantments) {
        if (item.getType().equals(Material.ENCHANTED_BOOK)) {
            final EnchantmentStorageMeta meta = (EnchantmentStorageMeta) item.getItemMeta();
            if (meta.getStoredEnchants() != null) {
                final ArrayList<Enchantment> clonestoredenchantments = new ArrayList<>(meta.getStoredEnchants().keySet());
                for (final Enchantment storedenchantment : clonestoredenchantments) {
                    meta.removeStoredEnchant(storedenchantment);
                }
            }
            for (final Entry<Enchantment, Integer> e : storedenchantments.entrySet()) {
                meta.addEnchant(e.getKey(), e.getValue(), true);
            }
            item.setItemMeta(meta);
        }
        return this;
    }

    public ItemCreator clearStoredEnchantments() {
        if (item.getType().equals(Material.ENCHANTED_BOOK)) {
            final EnchantmentStorageMeta meta = (EnchantmentStorageMeta) item.getItemMeta();
            if (meta.getStoredEnchants() != null) {
                final ArrayList<Enchantment> clonestoredenchantments = new ArrayList<>(meta.getStoredEnchants().keySet());
                for (final Enchantment storedenchantment : clonestoredenchantments) {
                    meta.removeStoredEnchant(storedenchantment);
                }
                item.setItemMeta(meta);
            }
        }
        return this;
    }

    public ItemCreator addStoredEnchantment(final Enchantment storedenchantment, final Integer lvl) {
        if (item.getType().equals(Material.ENCHANTED_BOOK)) {
            final EnchantmentStorageMeta meta = (EnchantmentStorageMeta) item.getItemMeta();
            meta.addStoredEnchant(storedenchantment, lvl, true);
            item.setItemMeta(meta);
        }
        return this;
    }

    public ItemCreator removeStoredEnchantment(final Enchantment enchantment) {
        if (item.getType().equals(Material.ENCHANTED_BOOK)) {
            final EnchantmentStorageMeta meta = (EnchantmentStorageMeta) item.getItemMeta();
            if (meta.getStoredEnchants() != null) {
                if (meta.getStoredEnchants().containsKey(enchantment)) {
                    meta.removeEnchant(enchantment);
                    item.setItemMeta(meta);
                }
            }
        }
        return this;
    }

    public Enchantment[] getTableauStoredEnchantments() {
        if (item.getType().equals(Material.ENCHANTED_BOOK)) {
            final EnchantmentStorageMeta meta = (EnchantmentStorageMeta) item.getItemMeta();
            final Enchantment[] storedenchantments = new Enchantment[] {};
            if (meta.getStoredEnchants() != null) {
                Integer i = 0;
                for (final Enchantment storedenchantment : meta.getStoredEnchants().keySet()) {
                    storedenchantments[i] = storedenchantment;
                    i++;
                }
            }
            return storedenchantments;
        }
        return null;
    }

    public Integer[] getTableauStoredEnchantmentslvl() {
        if (item.getType().equals(Material.ENCHANTED_BOOK)) {
            final EnchantmentStorageMeta meta = (EnchantmentStorageMeta) item.getItemMeta();
            final Integer[] storedenchantmentslvl = new Integer[] {};
            if (meta.getStoredEnchants() != null) {
                Integer i = 0;
                for (final Integer storedenchantmentlvl : meta.getStoredEnchants().values()) {
                    storedenchantmentslvl[i] = storedenchantmentlvl;
                    i++;
                }
            }
            return storedenchantmentslvl;
        }
        return null;
    }

    public ItemCreator setTableauStoredEnchantments(final Enchantment[] storedenchantments, final Integer[] storedenchantmentslvl) {
        if (item.getType().equals(Material.ENCHANTED_BOOK)) {
            final EnchantmentStorageMeta meta = (EnchantmentStorageMeta) item.getItemMeta();
            if (meta.getStoredEnchants() != null) {
                final ArrayList<Enchantment> clonestoredenchantments = new ArrayList<>(meta.getStoredEnchants().keySet());
                for (final Enchantment storedenchantment : clonestoredenchantments) {
                    meta.removeStoredEnchant(storedenchantment);
                }
            }
            for (Integer i = 0; i < storedenchantments.length && i < storedenchantmentslvl.length; i++) {
                meta.addEnchant(storedenchantments[i], storedenchantmentslvl[i], true);
            }
            item.setItemMeta(meta);
        }
        return this;
    }

    public ItemCreator addallItemsflags() {
        final ItemMeta meta = item.getItemMeta();
        meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
        meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        meta.addItemFlags(ItemFlag.HIDE_PLACED_ON);
        meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
        item.setItemMeta(meta);
        return this;
    }

    public ItemCreator addBannerPreset(final Integer ID, final DyeColor patterncolor) {
        switch (ID) {
            case 0:
                break;
            case 1:
                addBannerPreset(BannerPreset.barre, patterncolor);
                break;
            case 2:
                addBannerPreset(BannerPreset.precedent, patterncolor);
                break;
            case 3:
                addBannerPreset(BannerPreset.suivant, patterncolor);
                break;
            case 4:
                addBannerPreset(BannerPreset.coeur, patterncolor);
                break;
            case 5:
                addBannerPreset(BannerPreset.cercleEtoile, patterncolor);
                break;
            case 6:
                addBannerPreset(BannerPreset.croix, patterncolor);
                break;
            case 7:
                addBannerPreset(BannerPreset.yinYang, patterncolor);
                break;
            case 8:
                addBannerPreset(BannerPreset.losange, patterncolor);
                break;
            case 9:
                addBannerPreset(BannerPreset.moin, patterncolor);
                break;
            case 10:
                addBannerPreset(BannerPreset.plus, patterncolor);
                break;
            default:
                break;
        }
        return this;
    }

    public ItemCreator addBannerPreset(final BannerPreset type, final DyeColor patterncolor) {
        if (type == null)
            return this;
        if (item.getType().equals(bannerMat)) {
            final BannerMeta meta = (BannerMeta) item.getItemMeta();
            final DyeColor basecolor = meta.getBaseColor();
            switch (type) {
                // rien, barre, precedent, suivant, Coeur, cercleEtoile, croix,
                // yinYang, Losange, Moin, Plus;
                case barre:
                    addasyncronePattern(new Pattern(patterncolor, PatternType.STRIPE_DOWNRIGHT), true);
                    break;
                case precedent:
                    // precedent
                    addasyncronePattern(new Pattern(patterncolor, PatternType.RHOMBUS_MIDDLE), false);
                    addasyncronePattern(new Pattern(basecolor, PatternType.SQUARE_BOTTOM_RIGHT), false);
                    addasyncronePattern(new Pattern(basecolor, PatternType.SQUARE_TOP_RIGHT), false);
                    addasyncronePattern(new Pattern(basecolor, PatternType.STRIPE_RIGHT), true);
                    break;
                case suivant:
                    // suivant
                    addasyncronePattern(new Pattern(patterncolor, PatternType.RHOMBUS_MIDDLE), false);
                    addasyncronePattern(new Pattern(basecolor, PatternType.SQUARE_BOTTOM_LEFT), false);
                    addasyncronePattern(new Pattern(basecolor, PatternType.SQUARE_TOP_LEFT), false);
                    addasyncronePattern(new Pattern(basecolor, PatternType.STRIPE_LEFT), true);
                    break;
                case coeur:
                    // Coeur
                    addasyncronePattern(new Pattern(patterncolor, PatternType.RHOMBUS_MIDDLE), false);
                    addasyncronePattern(new Pattern(basecolor, PatternType.TRIANGLE_TOP), true);
                    break;
                case cercleEtoile:
                    // cercleEtoile
                    addasyncronePattern(new Pattern(patterncolor, PatternType.RHOMBUS_MIDDLE), false);
                    addasyncronePattern(new Pattern(patterncolor, PatternType.FLOWER), false);
                    addasyncronePattern(new Pattern(basecolor, PatternType.CIRCLE_MIDDLE), true);
                    break;
                case croix:
                    // croix
                    addasyncronePattern(new Pattern(patterncolor, PatternType.CROSS), false);
                    addasyncronePattern(new Pattern(basecolor, PatternType.CURLY_BORDER), true);
                    break;
                case yinYang:
                    // yinYang
                    addasyncronePattern(new Pattern(patterncolor, PatternType.SQUARE_BOTTOM_RIGHT), false);
                    addasyncronePattern(new Pattern(basecolor, PatternType.STRIPE_RIGHT), false);
                    addasyncronePattern(new Pattern(patterncolor, PatternType.DIAGONAL_LEFT), false);
                    addasyncronePattern(new Pattern(basecolor, PatternType.SQUARE_TOP_LEFT), false);
                    addasyncronePattern(new Pattern(patterncolor, PatternType.TRIANGLE_TOP), false);
                    addasyncronePattern(new Pattern(basecolor, PatternType.STRIPE_RIGHT), false);
                    addasyncronePattern(new Pattern(basecolor, PatternType.TRIANGLE_BOTTOM), false);
                    addasyncronePattern(new Pattern(patterncolor, PatternType.STRIPE_LEFT), true);
                    break;
                case losange:
                    // Losange
                    addasyncronePattern(new Pattern(patterncolor, PatternType.RHOMBUS_MIDDLE), true);
                    break;
                case moin:
                    // Moin
                    addasyncronePattern(new Pattern(patterncolor, PatternType.STRIPE_MIDDLE), false);
                    addasyncronePattern(new Pattern(basecolor, PatternType.BORDER), true);
                    break;
                case plus:
                    // Plus
                    addasyncronePattern(new Pattern(patterncolor, PatternType.STRAIGHT_CROSS), false);
                    addasyncronePattern(new Pattern(basecolor, PatternType.STRIPE_TOP), false);
                    addasyncronePattern(new Pattern(basecolor, PatternType.STRIPE_BOTTOM), false);
                    addasyncronePattern(new Pattern(basecolor, PatternType.BORDER), true);
                    break;
                default:
                    break;
            }
        }
        return this;
    }

    private void addasyncronePattern(final Pattern pattern, final Boolean calcul) {
        if (calcul) {
            patterns.add(pattern);
            final BannerMeta meta = (BannerMeta) item.getItemMeta();
            for (final Pattern currentpattern : patterns) {
                meta.addPattern(currentpattern);
            }
            patterns.clear();
            item.setItemMeta(meta);

        } else {
            if (patterns == null) {
                patterns = new ArrayList<>();
            }
            patterns.add(pattern);
        }
    }

    public Player getPossesseur() {
        return possesseur;
    }

    public ItemCreator setPossesseur(final Player possesseur) {
        this.possesseur = possesseur;
        return this;
    }

    public String getCreator_name() {
        return creator_name;
    }

    public ItemCreator setCreator_name(final String creator_name) {
        this.creator_name = creator_name;
        return this;
    }

    public ArrayList<String> getTag() {
        return tag;
    }

    public ItemCreator setTag(final ArrayList<String> tag) {
        this.tag = tag;
        return this;
    }

    public ItemCreator clearTag() {
        if (tag != null) {
            tag.clear();
        }
        return this;
    }

    public ItemCreator addTag(final String tag) {
        if (this.tag == null) {
            this.tag = new ArrayList<>();
        }
        this.tag.add(tag);
        return this;
    }

    public ItemCreator removeTag(final String tag) {
        if (this.tag != null) {
            if (this.tag.contains(tag)) {
                this.tag.remove(tag);
            }
        }
        return this;
    }

    public String[] getTableauTag() {
        final String[] taglist = new String[] {};
        Integer i = 0;
        for (final String currenttag : this.tag) {
            taglist[i] = currenttag;
            i++;
        }
        return taglist;
    }

    public ItemCreator setTableaTag(final String[] tag) {
        if (this.tag == null) {
            this.tag = new ArrayList<>();
        } else {
            this.tag.clear();
        }
        for (final String currenttag : tag) {
            this.tag.add(currenttag);
        }
        return this;
    }

    public Boolean comparate(final ItemCreator item, final ComparatorType type) {
        switch (type) {
            case All:
                return comparate(item, ComparatorType.Material) && comparate(item, ComparatorType.Amount)
                        && comparate(item, ComparatorType.Durability) && comparate(item, ComparatorType.Name)
                        && comparate(item, ComparatorType.Lores) && comparate(item, ComparatorType.Enchantements)
                        && comparate(item, ComparatorType.ItemsFlags) && comparate(item, ComparatorType.Owner)
                        && comparate(item, ComparatorType.BaseColor) && comparate(item, ComparatorType.Patterns)
                        && comparate(item, ComparatorType.StoredEnchantements)
                        && comparate(item, ComparatorType.Creator_Name) && comparate(item, ComparatorType.Possesseur)
                        && comparate(item, ComparatorType.TAG);
            case Similar:
                return comparate(item, ComparatorType.Material) && comparate(item, ComparatorType.Durability)
                        && comparate(item, ComparatorType.Name) && comparate(item, ComparatorType.Lores)
                        && comparate(item, ComparatorType.Enchantements) && comparate(item, ComparatorType.ItemsFlags)
                        && comparate(item, ComparatorType.Owner) && comparate(item, ComparatorType.BaseColor)
                        && comparate(item, ComparatorType.Patterns) && comparate(item, ComparatorType.StoredEnchantements);
            case ItemStack:
                return comparate(item, ComparatorType.Material) && comparate(item, ComparatorType.Amount)
                        && comparate(item, ComparatorType.Durability) && comparate(item, ComparatorType.Name)
                        && comparate(item, ComparatorType.Lores) && comparate(item, ComparatorType.Enchantements)
                        && comparate(item, ComparatorType.ItemsFlags) && comparate(item, ComparatorType.Owner)
                        && comparate(item, ComparatorType.BaseColor) && comparate(item, ComparatorType.Patterns)
                        && comparate(item, ComparatorType.StoredEnchantements);
            case Material:
                return getMaterial() == item.getMaterial();
            case Amount:
                return getAmount() == item.getAmount();
            case Durability:
                return getDurability() == item.getDurability();
            case Name:
                return getName() == item.getName();
            case Lores:
                return new comparaison<String, Object>().islistequal(getLores(), item.getLores());
            case Enchantements:
                return new comparaison<Enchantment, Integer>().ismapequal(getEnchantments(), item.getEnchantments());
            case ItemsFlags:
                return new comparaison<ItemFlag, Object>().islistequal(getItemFlags(), item.getItemFlags());
            case Owner:
                return getOwner() == item.getOwner();
            case BaseColor:
                return getBasecolor() == item.getBasecolor();
            case Patterns:
                return new comparaison<Pattern, Object>().islistequal(getPatterns(), item.getPatterns());
            case StoredEnchantements:
                return new comparaison<Enchantment, Integer>().ismapequal(getStoredEnchantments(),
                        item.getStoredEnchantments());
            case Possesseur:
                return getPossesseur() == item.getPossesseur();
            case Creator_Name:
                return getCreator_name() == item.getCreator_name();
            case TAG:
                return new comparaison<String, Object>().islistequal(getTag(), item.getTag());
            default:
                return false;
        }
    }

    private class comparaison<type1, type2> {
        public comparaison() {

        }

        public Boolean islistequal(final List<type1> list1, final List<type1> list2) {
            if (list1 == null && list2 == null) {
                return true;
            } else if (list1 == null || list2 == null) {
                return false;
            } else if (list1.size() == list2.size()) {
                for (Integer i = 0; i < list1.size() && i < list2.size(); i++) {
                    if (list1.get(i) != list2.get(i)) {
                        return false;
                    }
                }
                return true;
            } else {
                return false;
            }
        }

        public Boolean ismapequal(final Map<type1, type2> map1, final Map<type1, type2> map2) {
            if (map1 == null && map2 == null) {
                return true;
            } else if (map1 == null || map2 == null) {
                return false;
            } else if (map1.size() == map2.size()) {
                for (final Entry<type1, type2> e : map1.entrySet()) {
                    if (map2.get(e.getKey()) == null) {
                        return false;
                    }
                    if (map2.get(e.getKey()) != e.getValue()) {
                        return false;
                    }
                }
                return true;
            } else {
                return false;
            }
        }
    }
}
