package fr.ChadOW.cinventory.interfaces.events.clickContent;


public class ClickContext {
    private final Action inventoryAction;

    private final ClickType clickType;

    public ClickContext(Action inventoryAction, ClickType clickType) {
        this.inventoryAction = inventoryAction;
        this.clickType = clickType;
    }

    public Action getInventoryAction() {
        return this.inventoryAction;
    }

    public ClickType getClickType() {
        return this.clickType;
    }
}
