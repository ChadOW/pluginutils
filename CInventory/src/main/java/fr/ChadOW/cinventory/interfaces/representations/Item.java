package fr.ChadOW.cinventory.interfaces.representations;

import fr.ChadOW.cinventory.interfaces.events.ItemClickEvent;
import org.bukkit.inventory.ItemStack;

import java.util.List;

public interface Item {
    int getSlot();
    List<ItemClickEvent> getEvents();
    List<Inventory> getInventories();

    ItemStack build();
}
