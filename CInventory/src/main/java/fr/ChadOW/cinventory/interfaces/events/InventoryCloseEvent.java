package fr.ChadOW.cinventory.interfaces.events;

import fr.ChadOW.cinventory.interfaces.representations.Inventory;
import org.bukkit.entity.Player;

public interface InventoryCloseEvent {
    void onClick(Inventory paramInventory, Player paramPlayer);
}
