package fr.ChadOW.cinventory.interfaces.events;

import fr.ChadOW.cinventory.interfaces.representations.Inventory;
import fr.ChadOW.cinventory.interfaces.events.clickContent.ClickContext;
import fr.ChadOW.cinventory.interfaces.representations.Item;
import org.bukkit.entity.Player;

public interface ItemClickEvent {
    void onClick(Inventory paramInventory, Item paramItem, Player paramPlayer, ClickContext paramClickContext);
}
